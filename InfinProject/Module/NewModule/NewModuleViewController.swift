//
//  NewModuleViewController.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

class NewModuleViewController: BaseViewController {
    
    let viewModel = NewModuleViewModel()
    private let router = NewModuleRouter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearence()
    }
    
    private func reloadView() {
    }
    
    
    
    // MARK: Appearence
    
    private func setupAppearence() {
    
    }
}
