//
//  NewModuleRouter.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

protocol Router {
    associatedtype RouteType
    
    func route(
        to routeID: RouteType,
        from context: UIViewController
    )
}

enum NewModuleRouterIdentifiers {
    case back
}

class NewModuleRouter: Router {
    func route(to routeID: NewModuleRouterIdentifiers, from context: UIViewController) {
        switch routeID {
        case .back:
            context.navigationController?.popViewController(animated: true)
            break
        }
    }
}
