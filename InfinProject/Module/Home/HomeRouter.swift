//
//  NewModuleRouter.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit
import SafariServices

enum HomeRouterIdentifiers {
    case back
    case openUrl(_ url:URL)
    case logOut
}

class HomeRouter: Router {
    func route(to routeID: HomeRouterIdentifiers, from context: UIViewController) {
        switch routeID {
        case .back:
            context.navigationController?.popViewController(animated: true)
            break
        case .openUrl(let url):
            let controller = SFSafariViewController(url: url)
            context.present(controller, animated: true, completion: nil)
        case .logOut:
            UserManager.shared.bearerToken = nil
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.openLogin()
            break
        }
    }
}
