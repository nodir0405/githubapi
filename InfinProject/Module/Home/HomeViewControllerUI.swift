//
//
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit

protocol HomeViewControllerUI: ViewInstaller {
    var tableView : UITableView! {get set}
    var searchField : UITextField! {get set}
    var logOut : UIButton! {get set}
}

extension HomeViewControllerUI{
    func initSubviews() {
        searchField = {
            let field = UITextField()
            field.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            field.backgroundColor = UIColor.clear
            field.backgroundColor = .white
            field.layer.cornerRadius = 4
            field.layer.borderWidth = 1
            field.autocorrectionType = .no
            field.placeholder = "Search repository"
            field.layer.borderColor = UIColor.gray.cgColor
            field.addLeft(left: 5)
            return field
        }()
        logOut = {
            let btn = UIButton()
            btn.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            btn.backgroundColor = .clear
            btn.setTitleColor(.black, for: .normal)
            btn.setTitle("LogOut", for: .normal)
            return btn
        }()
        tableView = {
            let tableView = UITableView()
            return tableView
        }()
    }
    
    func embedSubviews() {
        mainView.addSubview(searchField)
        mainView.addSubview(tableView)
    }
        
    func addSubviewsConstraints() {
        var top:CGFloat = 0.0
        if let params = self.parameter as? [String :CGFloat]{
            top = params["top"] ?? 0
        }
        searchField.snp.makeConstraints { make in
            make.top.equalTo(top)
            make.left.equalTo(30)
            make.right.equalTo(-20)
            make.height.equalTo(50)
        }
        tableView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(searchField.snp.bottom).offset(20)
        }
    }
   
   
}
