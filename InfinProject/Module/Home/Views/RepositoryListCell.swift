//
//  RepositoryListCell.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import SDWebImage

class RepositoryListCell: BaseTableViewCell {

    private  let avatarView = UIImageView()
    private let nameLabel = UILabel()
    private let languageLabel = UILabel()
    private let stackView = UIStackView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    override func initSubviews() {
        stackView.axis = .vertical
        nameLabel.numberOfLines = 2
        languageLabel.textColor = .green
    }
    override func embedSubviews() {
        contentView.addSubview(avatarView)
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(languageLabel)
    }
    override func addSubviewsConstraints() {
        avatarView.snp.makeConstraints { make in
            make.height.width.equalTo(50)
            make.left.equalTo(15)
            make.top.equalTo(5)
            make.bottom.equalTo(-5)
        }
        stackView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(avatarView.snp.right).offset(10)
            make.right.equalTo(-10)
        }
    }
    
    func setupRepository(_ repo: RepositoryRO?){
        self.nameLabel.text = repo?.name
        self.languageLabel.text = repo?.language
        self.avatarView.sd_setImage(with: URL(string: repo?.owner?.avatar ?? ""),
                                    placeholderImage: UIImage(named: "avatar_placeholder"),
                                    options: [], context: nil)
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
