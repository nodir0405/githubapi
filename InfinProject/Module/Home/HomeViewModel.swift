//
//  NewModuleViewModel.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

class HomeViewModel {
    
    private var page = 1
    private var totalCounts = 1000
    private var perpage = 50
    private var loading = false
    private let network = Network.shared
    var repositoryItems = [RepositoryRO]()
    
    func search(search: String, onSuccess: @escaping ()->(), onError: @escaping (AppError)->()) {
        page = 1
        _ = network.request(target: AppEndpoint.nextPage(searchText: search, page: page, count: perpage),
                        success: { [weak self ](result, jsonString) in
                            if let response = result as? RepositoryResponse {
                                self?.page = 2
                                self?.totalCounts = response.total_count
                                self?.repositoryItems = response.items ?? []
                                onSuccess()
                            } else {
                                onError(AppError.unknown())
                            }
                        }) { (error) in
            onError(error)
        }
    }
    func nextPage(search: String, onSuccess: @escaping ()->(), onError: @escaping (AppError)->()) {
        if loading || repositoryItems.count >= totalCounts{
            return
        }
        loading = true
        _ = network.request(target: AppEndpoint.nextPage(searchText: search, page: page, count: perpage),
                        success: { [weak self ](result, jsonString) in
                            if let response = result as? RepositoryResponse {
                                let items = response.items ?? []
                                self?.loading = false
                                self?.page = (self?.page ?? 0 ) + 1
                                self?.repositoryItems.append(contentsOf: items)
                                onSuccess()
                            } else {
                                onError(AppError.unknown())
                            }
                        }) { (error) in
            onError(error)
        }
    }
}
