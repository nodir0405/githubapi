//
//  NewModuleViewController.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit
import SafariServices
import FirebaseAuth


class HomeViewController: BaseViewController, HomeViewControllerUI {
    var logOut: UIButton!
    
    var parameter: Any?
    
    var searchField: UITextField!
    
    var tableView: UITableView!
    var mainView: UIView { view }
    
    
    let viewModel = HomeViewModel()
    private let router = HomeRouter()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let top:CGFloat = UIApplication.shared.statusBarFrame.size.height +
            (navigationController?.navigationBar.frame.height ?? 0.0) + 10
        self.parameter = ["top":top]
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset = .zero
        tableView.tableFooterView = UIView()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(search), for: .valueChanged)
        tableView.register(RepositoryListCell.self, forCellReuseIdentifier: "RepositoryListCell")
        searchField.addTarget(self, action: #selector(changeSearchText(sender:)), for: .editingChanged)
        logOut.addTarget(self, action: #selector(logOutClicked), for: .touchUpInside)
    }
    
    @objc func changeSearchText(sender:UITextField){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector:#selector(search) , object: nil)
        self.perform(#selector(search), with: nil, afterDelay: 0.5)
    }
    
    @objc func search(){
        let searchText = searchField.text ?? ""
        if searchText.count > 2{
            viewModel.search(search: searchText) { [weak self] in
                self?.refreshControl.endRefreshing()
                self?.tableView.reloadData()
            } onError: { [weak self] error in
                self?.showError(error: error)
            }
        } else{
            refreshControl.endRefreshing()
        }
        
    }
    func nextpage(){
        let searchText = searchField.text ?? ""
        viewModel.nextPage(search: searchText) { [weak self] in
            self?.tableView.reloadData()
        } onError: {[weak self] error in
            self?.showError(error: error)
        }
    }
    @objc func logOutClicked(){
        do{
            try Auth.auth().signOut()
            router.route(to: .logOut, from: self)
        }catch{
            self.showError(error: AppError(JSON: ["message":"Error while signing out!"])!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: logOut)
    }
}
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositoryItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryListCell") as! RepositoryListCell
        cell.setupRepository(viewModel.repositoryItems[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= viewModel.repositoryItems.count - 3{
            nextpage()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.repositoryItems[indexPath.row]
        let urlString = item.owner?.url
        if let url = URL(string: urlString ?? ""){
            router.route(to: .openUrl(url), from: self)
        }
        
    }
}
