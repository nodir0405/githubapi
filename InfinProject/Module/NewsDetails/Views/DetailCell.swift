//
//  NewsListCell.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 03/08/21.
//

import UIKit

class DetailCell: BaseTableViewCell {
    private let attachView = UIImageView()
    private let dateLabel = UILabel()
    private let titleLabel = UILabel()
    private let textView = UITextView()
    
    override func initSubviews() {
        attachView.contentMode = .scaleAspectFill
        attachView.clipsToBounds = true
        
        dateLabel.font = UIFont.helveticaNeue(size: 14)
        dateLabel.textColor = .gray
        
        titleLabel.font = UIFont.helveticaBold(size: 15)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
     
        //textView.font = UIFont.helveticaNeue(size: 14)
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.linkTextAttributes = [.foregroundColor : UIColor.blue]
    }
    override func embedSubviews() {
        contentView.addSubview(attachView)
        contentView.addSubview(dateLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(textView)
    }
    override func addSubviewsConstraints() {
        let padding = 10
        attachView.snp.makeConstraints { make in
            make.top.equalTo(10)
            make.left.equalTo(padding)
            make.right.equalTo(-padding)
            make.height.equalTo(attachView.snp.width).multipliedBy(0.66)
        }
        dateLabel.snp.makeConstraints { make in
            make.left.equalTo(padding)
            make.right.equalTo(-padding)
            make.top.equalTo(attachView.snp.bottom).offset(10)
        }
        titleLabel.snp.makeConstraints { make in
            make.left.equalTo(dateLabel)
            make.right.equalTo(-padding)
            make.top.equalTo(dateLabel.snp.bottom).offset(5)
        }
        textView.snp.makeConstraints { make in
            make.left.equalTo(dateLabel)
            make.right.equalTo(-padding)
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.bottom.equalTo(-20)
            make.height.greaterThanOrEqualTo(50)
        }
        
    }
    func setupNews(_ news:NewsResponse?){
        self.attachView.sd_setImage(with: URL(string: news?.thumbnail ?? ""),
                                    placeholderImage: UIImage(named: "avatar_placeholder"),
                                    options: [], context: nil)
        dateLabel.text = news?.dateString
        titleLabel.text = news?.title
        textView.attributedText = news?.htmlContent
    }
}
