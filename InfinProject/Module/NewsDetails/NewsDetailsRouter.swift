//
//  NewModuleRouter.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

enum NewsDetailsRouterIdentifiers {
    case back
}

class NewsDetailsRouter: Router {
    func route(to routeID: NewsDetailsRouterIdentifiers, from context: UIViewController) {
        switch routeID {
        case .back:
            print("context")
        }
    }
}

