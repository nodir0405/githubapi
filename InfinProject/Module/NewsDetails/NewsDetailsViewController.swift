//
//  NewModuleViewController.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit
import FirebaseAuth

class NewsDetailsViewController: BaseViewController, NewsDetailsViewControllerUI  {
    var tableView: UITableView!
    
    var mainView: UIView { view }
    var parameter: Any?
    
    let viewModel = NewsDetailsViewModel()
    private let router = NewsDetailsRouter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(DetailCell.self, forCellReuseIdentifier: "DetailCell")
        let data = Data((viewModel.news?.content ?? "").utf8)

        if viewModel.news?.htmlContent == nil{
            if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
                self.viewModel.news?.htmlContent = attributedString
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
  
}
extension NewsDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
        cell.setupNews(viewModel.news)
        cell.selectionStyle = .none
        return cell
    }
    
}
