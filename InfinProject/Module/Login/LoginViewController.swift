//
//  NewModuleViewController.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit
import FirebaseAuth

class LoginViewController: BaseViewController, LoginViewControllerUI  {
    var newsBtn: UIButton!
    
    
    var parameter: Any?
    
    var loginLabel: UILabel!
    
    var loginField: UITextField!
    
    var passwordLabel: UILabel!
    
    var passwordField: UITextField!
    
    var loginBtn: UIButton!
    
    var mainView: UIView { view }
    
    let viewModel = LoginViewModel()
    private let router = LoginRouter()
    
    let provider = OAuthProvider(providerID: "github.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        loginBtn.addTarget(self, action: #selector(loginClicked), for: .touchUpInside)
        newsBtn.addTarget(self, action: #selector(newsClicked), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     //   navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc func loginClicked(){
        self.gitHubAuth()
    }
    @objc func newsClicked(){
        router.route(to: .news, from: self)
    }
    
    func gitHubAuth(){
        
        provider.customParameters = [
              "allow_signup": "false"
            ]
    
        provider.getCredentialWith(nil) { [weak self] credential, error in
            if error != nil {
                self?.showError(error: AppError(JSON: ["message":"Auth Error"])!)
            }
            if let credent = credential{
                Auth.auth().signIn(with: credent) { result, error in
                    if error != nil {
                        self?.showError(error: AppError(JSON: ["message":"Auth Error"])!)
                        return
                    }
                    guard let oauthCredential = result?.credential as? OAuthCredential else { return }
                    //print(oauthCredential.accessToken)
                    UserManager.shared.bearerToken = oauthCredential.accessToken
                    self?.successLogin()
                }
            
            }
        }
    }
    
    @objc func successLogin(){
        router.route(to: .home, from: self)
    }
  
}
