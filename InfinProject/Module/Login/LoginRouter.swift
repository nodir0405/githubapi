//
//  NewModuleRouter.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

enum LoginRouterIdentifiers {
    case back
    case home
    case news
}

class LoginRouter: Router {
    func route(to routeID: LoginRouterIdentifiers, from context: UIViewController) {
        switch routeID {
        case .back:
            context.navigationController?.popViewController(animated: true)
            break
        case .home:
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.openHome()
            break
        case .news:
            context.navigationController?.pushViewController(NewsViewController(), animated: true)
            break
        }
    }
}
