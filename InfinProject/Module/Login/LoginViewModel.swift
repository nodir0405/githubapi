//
//  NewModuleViewModel.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

class LoginViewModel {
    
    private let network = Network.shared
    
    func login(username: String, password:String, onSuccess: @escaping ()->(), onError: @escaping (AppError)->()) {
        
        _ = network.request(target: AppEndpoint.login(username: username, password: password),
                        success: { [weak self ](result, jsonString) in
                            if let response = result as? APIBaseObject {
                                print(response)
                                onSuccess()
                            } else {
                                onError(AppError.unknown())
                            }
                        }) { (error) in
            onError(error)
        }
    }
}
