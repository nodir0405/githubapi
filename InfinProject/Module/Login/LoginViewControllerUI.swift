//
//  LoginViewControllerUI.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit

protocol LoginViewControllerUI: ViewInstaller {
    var loginLabel: UILabel! { get set }
    var loginField: UITextField! {get set}
    var passwordLabel: UILabel! { get set }
    var passwordField: UITextField! {get set}
    var loginBtn: UIButton! {get set}
    var newsBtn: UIButton! {get set}
    
}

extension LoginViewControllerUI{
    func initSubviews() {
        loginLabel = {
            let label = UILabel()
            label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            label.backgroundColor = UIColor.clear
            label.textColor = UIColor.systemGray
            label.textAlignment = .center
            label.text = "Login"
            return label
        }()
        
        loginField = {
            let field = UITextField()
            field.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            field.backgroundColor = .white
            field.layer.cornerRadius = 4
            field.layer.borderWidth = 1
            field.layer.borderColor = UIColor.gray.cgColor
            field.addLeft(left: 10)
            return field
        }()
        passwordLabel = {
            let label = UILabel()
            label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            label.backgroundColor = UIColor.clear
            label.textColor = UIColor.systemGray
            label.textAlignment = .center
            label.text = "Password"
            return label
        }()
        passwordField = {
            let field = UITextField()
            field.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            field.backgroundColor = .white
            field.isSecureTextEntry = true
            field.layer.cornerRadius = 4
            field.layer.borderWidth = 1
            field.layer.borderColor = UIColor.gray.cgColor
            field.addLeft(left: 10)
            return field
        }()
        loginBtn = {
            let btn = UIButton()
            btn.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            btn.backgroundColor = .white
            btn.layer.cornerRadius = 4
            btn.layer.borderWidth = 1
            btn.layer.borderColor = UIColor.gray.cgColor
            btn.setTitleColor(.black, for: .normal)
            btn.setTitle("GitHub", for: .normal)
            return btn
        }()
        newsBtn = {
            let btn = UIButton()
            btn.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            btn.backgroundColor = .white
            btn.layer.cornerRadius = 4
            btn.layer.borderWidth = 1
            btn.layer.borderColor = UIColor.gray.cgColor
            btn.setTitleColor(.black, for: .normal)
            btn.setTitle("News", for: .normal)
            return btn
        }()
        
    }
    func embedSubviews() {
        mainView.addSubview(loginLabel)
        mainView.addSubview(loginField)
        mainView.addSubview(passwordLabel)
        mainView.addSubview(passwordField)
        mainView.addSubview(loginBtn)
        mainView.addSubview(newsBtn)
        
        loginField.isHidden = true
        loginLabel.isHidden = true
        passwordField.isHidden = true
        passwordLabel.isHidden = true
    }
        
    func addSubviewsConstraints() {
        loginLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview().offset(-300)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(25)
        }
        loginField.snp.makeConstraints { make in
            make.top.equalTo(loginLabel.snp.bottom).offset(20)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(45)
        }
        passwordLabel.snp.makeConstraints { make in
            make.top.equalTo(loginField.snp.bottom).offset(20)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(25)
        }
        passwordField.snp.makeConstraints { make in
            make.top.equalTo(passwordLabel.snp.bottom).offset(20)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(45)
        }
        loginBtn.snp.makeConstraints { make in
            make.top.equalTo(passwordField.snp.bottom).offset(20)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(45)
        }
        newsBtn.snp.makeConstraints { make in
            make.top.equalTo(loginBtn.snp.bottom).offset(20)
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.height.equalTo(45)
        }
        
    }
    
   
}
