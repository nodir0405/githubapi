//
//  NewModuleViewModel.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

class NewsViewModel {
    
    private var page = 0
    private var perpage = 15
    private var loading = false
    private var complete = false
    private let network = Network.shared
    
    var newsItems = [NewsResponse]()
    
    func getNews(onSuccess: @escaping ()->(), onError: @escaping (AppError)->()) {
        page = 0
        complete = false
        _ = network.request2(target: NewsEndpoint.news(offset: page, count: perpage),
                        success: { [weak self ](result, jsonString) in
                            if let response = result as? [NewsResponse] {
                                self?.page = 1
                                let perpage = self?.perpage ?? 0
                                self?.complete = response.count < perpage
                                self?.newsItems = response
                                onSuccess()
                            } else {
                                onError(AppError.unknown())
                            }
                        }) { (error) in
            onError(error)
        }
    }
    func nextPage(onSuccess: @escaping ()->(), onError: @escaping (AppError)->()) {
        if loading || complete{
            return
        }
        loading = true
        _ = network.request2(target: NewsEndpoint.news(offset: page, count: perpage),
                        success: { [weak self ](result, jsonString) in
                            if let response = result as? [NewsResponse] {
                                self?.loading = false
                                self?.page = (self?.page ?? 0 ) + 1
                                self?.newsItems.append(contentsOf: response)
                                let perpage = self?.perpage ?? 0
                                self?.complete = response.count < perpage
                                onSuccess()
                            } else {
                                onError(AppError.unknown())
                            }
                        }) { (error) in
            onError(error)
        }
    }
}
