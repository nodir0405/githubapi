//
//  LoginViewControllerUI.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit

protocol NewsViewControllerUI: ViewInstaller {
    var tableView: UITableView! {get set}
    var refresh: UIRefreshControl! {get set} 
}

extension NewsViewControllerUI{
    func initSubviews() {
        tableView = {
            let tableView = UITableView()
            tableView.separatorInset = .zero
            tableView.separatorStyle = .none
            tableView.tableFooterView = UIView()
            return tableView
        }()
        refresh = {
          return UIRefreshControl()
        }()
    }
    func embedSubviews() {
        mainView.addSubview(tableView)
    }
        
    func addSubviewsConstraints() {
        tableView.snp.makeConstraints { make in
            make.left.right.top.bottom.equalToSuperview()
        }
    }
    
   
}
