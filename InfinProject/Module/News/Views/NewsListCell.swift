//
//  NewsListCell.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 03/08/21.
//

import UIKit

class NewsListCell: BaseTableViewCell {
    private let attachView = UIImageView()
    private let dateLabel = UILabel()
    private let titleLabel = UILabel()
    private let lineView = UIView()
    
    override func initSubviews() {
        attachView.contentMode = .scaleAspectFill
        attachView.clipsToBounds = true
        
        dateLabel.font = UIFont.helveticaNeue(size: 14)
        dateLabel.textColor = .gray
        
        titleLabel.font = UIFont.helveticaBold(size: 15)
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 0
        
        lineView.backgroundColor = UIColor.blue.withAlphaComponent(0.7)
    }
    override func embedSubviews() {
        contentView.addSubview(lineView)
        contentView.addSubview(attachView)
        contentView.addSubview(dateLabel)
        contentView.addSubview(titleLabel)
    }
    override func addSubviewsConstraints() {
        let attachH = UIScreen.main.bounds.size.width/4
        let padding = 10
        lineView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.left.equalTo(padding)
            make.height.equalTo(15)
            make.right.equalTo(-padding)
        }
        attachView.snp.makeConstraints { make in
            make.height.width.equalTo(attachH)
            make.top.equalTo(lineView.snp.bottom).offset(10)
            make.left.equalTo(padding)
            make.bottom.equalTo(-5)
        }
        dateLabel.snp.makeConstraints { make in
            make.left.equalTo(attachView.snp.right).offset(10)
            make.top.equalTo(attachView)
        }
        titleLabel.snp.makeConstraints { make in
            make.left.equalTo(dateLabel)
            make.right.equalTo(-padding)
            make.top.equalTo(dateLabel.snp.bottom).offset(5)
        }
    }
    func setupNews(_ news:NewsResponse){
        self.attachView.sd_setImage(with: URL(string: news.thumbnail ?? ""),
                                    placeholderImage: UIImage(named: "avatar_placeholder"),
                                    options: [], context: nil)
        dateLabel.text = news.dateString
        titleLabel.text = news.title
    }
}
