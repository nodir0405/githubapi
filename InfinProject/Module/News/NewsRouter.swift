//
//  NewModuleRouter.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit

enum NewsRouterIdentifiers {
    case details(_ news:NewsResponse)
}

class NewsRouter: Router {
    func route(to routeID: NewsRouterIdentifiers, from context: UIViewController) {
        switch routeID {
        case .details(let news):
            let controller = NewsDetailsViewController()
            controller.viewModel.news = news
            context.navigationController?.pushViewController(controller, animated: true)
            break
        }
    }
}
