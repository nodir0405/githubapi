//
//  NewModuleViewController.swift
//  iOSApp
//
//  Created by iOS developer on 27.05.2021.
//

import UIKit
import FirebaseAuth

class NewsViewController: BaseViewController, NewsViewControllerUI  {
    var refresh: UIRefreshControl!
    var tableView: UITableView!
    
    var mainView: UIView { view }
    var parameter: Any?
    
    let viewModel = NewsViewModel()
    private let router = NewsRouter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(NewsListCell.self, forCellReuseIdentifier: "NewsListCell")
        tableView.addSubview(refresh)
        refresh.addTarget(self, action: #selector(self.loadData), for: .valueChanged)
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @objc func loadData(){
        viewModel.getNews {[weak self] in
            self?.reloadAllData()
        } onError: { [weak self] error in
            self?.showError(error: error)
        }

    }
    
    func nextPage(){
        viewModel.nextPage {[weak self] in
            self?.reloadAllData()
        } onError: { [weak self] error in
            self?.showError(error: error)
        }

    }
    
    func reloadAllData(){
        refresh.endRefreshing()
        self.tableView.reloadData()
    }
    
  
}
extension NewsViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.newsItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsListCell") as! NewsListCell
        let item = viewModel.newsItems[indexPath.row]
        cell.setupNews(item)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router.route(to: .details(viewModel.newsItems[indexPath.row]), from: self)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= viewModel.newsItems.count - 3{
            nextPage()
        }
    }
}
