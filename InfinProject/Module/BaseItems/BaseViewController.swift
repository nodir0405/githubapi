//
//  BaseViewController.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import JGProgressHUD
import Toast_Swift

class BaseViewController: UIViewController {
    private  let hud = JGProgressHUD(style: .light)
    var toastStyleError = ToastStyle()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
    }
   
    func showLoader(_ show:Bool){
        if show{
            hud.show(in: navigationController?.view ?? view)
        } else{
            hud.dismiss()
        }
    }
        
        
    func showError(error: AppError) {
        showError(error: error, completion: {})
    }
    
    func showError(error: AppError, completion: @escaping ()->()) {
        
        if error.responseCode ?? 0 >= 500 || error.responseCode == nil {
            self.view.makeToast(error.message, duration: 2, position: .bottom, title: nil, image: nil, style: toastStyleError) { isTimeEnd in}
            completion()
        } else {
            let message: String
            if (error.message ?? "").contains("error.pin.invalid") {
                message = "error.pin.invalid".localized + " " + (error.message?.split(separator: ".").last ?? "")
            } else if error.message == "error.phone_blocked" || error.message == "error.sms_flood" {
                let blokedDate = Date(timeIntervalSince1970: TimeInterval(error.extraMessage ?? 0))
                let seconds = max(blokedDate.timeIntervalSince(Date()), 0)
                message = String(format: error.message?.localized ?? "", Int(seconds / 60 + 1))
            } else {
                message = error.message?.localized ?? ""
            }
            
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Common.Ok".localized, style: .default, handler: { _ in
                completion()
            }))
            self.present(alert, animated: true)
        }
    }
}
