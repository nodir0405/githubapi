//
//  BaseView.swift
//  iOSApp
//
//  Created by Jurayev Nodir on 14/07/21.
//

import UIKit
import SnapKit

class BaseView: UIView {

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        setupSubviews()
    }
    func setupSubviews() {
        initSubviews()
        embedSubviews()
        addSubviewsConstraints()
    }
    
    func initSubviews() {
        //fatalError("Implementation pending...")
    }
    
    func embedSubviews() {
        //fatalError("Implementation pending...")
    }
    
    func addSubviewsConstraints() {
        //fatalError("Implementation pending...")
    }
    
    func localizeText() {
        //fatalError("Implementation pending...")
    }

}




class TestCell: BaseTableViewCell{
    let label = UILabel()
    
    override func initSubviews() {
        //label.font
    }
    override func embedSubviews() {
        contentView.addSubview(label)
    }
    override func addSubviewsConstraints() {
        label.snp.makeConstraints { (make) in
            make.left.top.equalTo(10)
            make.bottom.right.equalTo(-10)
            make.height.equalTo(25)
        }
    }
}

