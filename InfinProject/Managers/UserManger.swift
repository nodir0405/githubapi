//
//  UserManger.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import KeychainSwift

class UserManager {
    static let shared = UserManager()
    
    private let keychain = KeychainSwift()
    private let network = Network.shared
    
    private (set) var userId: Int?{
        get {
            Int(keychain.get(Keychain.userId) ?? "0")
        }
        set {
            if let id = newValue {
                keychain.set("\(id)", forKey: Keychain.userId)
            } else {
                keychain.delete(Keychain.userId)
            }
        }
    }
    
    var bearerToken: String? {
        get {
            let token = keychain.get(Keychain.bearerToken)
            return token
        }
        set {
            if let token = newValue {
                keychain.set(token, forKey: Keychain.bearerToken)
            } else {
                keychain.delete(Keychain.bearerToken)
            }
        }
    }
    var refreshToken: String? {
        get {
            keychain.get(Keychain.refreshToken)
        }
        set {
            if let token = newValue {
                keychain.set(token, forKey: Keychain.refreshToken)
            } else {
                keychain.delete(Keychain.refreshToken)
            }
        }
    }
    
}
