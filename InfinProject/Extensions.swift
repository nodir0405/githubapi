//
//  Extensions.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import Foundation
import Alamofire


public enum Language: String {
    case english = "English"
    case russian = "Русский"
    case uzbek = "O'zbek tilida"
    
    public static let all: [Language] = [.uzbek, .russian, .english]
    
    public var code: String {
        switch self {
        case .english: return "en"
        case .russian: return "ru"
        case .uzbek: return "uz"
        }
    }
}

extension UserDefaults {
    enum Key: String {
        case reviewWorthyActionCount
        case lastReviewRequestAppVersion
    }
    
    func integer(forKey key: Key) -> Int {
        return integer(forKey: key.rawValue)
    }
    
    func string(forKey key: Key) -> String? {
        return string(forKey: key.rawValue)
    }
    
    func set(_ integer: Int, forKey key: Key) {
        set(integer, forKey: key.rawValue)
    }
    
    func set(_ object: Any?, forKey key: Key) {
        set(object, forKey: key.rawValue)
    }
}

extension NSNotification.Name {
    enum Notifications: String {
        case cardUpdates, userUpdates, favoriteUpdates, appModeUpdates, changeLanguage, serviceDraging
    }
    
    init(_ value: Notifications) {
        self = NSNotification.Name(value.rawValue)
    }
}


extension UIColor {
    public convenience init?(r: CGFloat, g:CGFloat, b:CGFloat) {
        self.init(red: r / 255.0,
                  green: g / 255.0,
                  blue: b / 255.0,
                  alpha: CGFloat(1.0))
    }
    public convenience init?(hex: String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return nil
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: CGFloat(1.0))
    }
}
extension UIFont {
//    public convenience init!(size: CGFloat, bold:Bool) {
//        if bold {
//            self.init(name: "Gilroy-Bold", size: size)
//        } else{
//            self.init(name: "Gilroy-Medium", size: size)
//        }
//    }
    public static func helveticaNeue(size:CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Thin", size: size)!
    }
    public static func helveticaBold(size:CGFloat) -> UIFont{
        return UIFont(name: "HelveticaNeue-Bold", size: size)!
    }
    
//    public static func Semibold(size:CGFloat) -> UIFont{
//        return UIFont(name: "Gilroy-Semibold", size: size)!
//    }
//
//    public static func Regular(size:CGFloat) -> UIFont{
//        return UIFont(name: "Gilroy-Regular", size: size)!
//    }
//    public static func Medium(size:CGFloat) -> UIFont{
//        return UIFont(name: "Gilroy-Medium", size: size)!
//    }
    
}
extension Dictionary where Key == String {
    
    func doubleValue(_ key:String)-> Double{
        if let value = self[key] as? NSObject{
            let str = String(format: "%@", value)
            return Double(str) ?? 0.0
        }
        return 0.0
    }
    func intValue(_ key:String)-> Int{
        if let value = self[key] as? NSObject{
            let str = String(format: "%@", value)
            return Int(str) ?? 0
        }
        return 0
    }
}
extension Date {
    public func string(format: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en")
        return dateFormatter.string(from: self)
    }
    static public func date(string: String?, format: String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en")
        return dateFormatter.date(from: string ?? "") ?? Date()
    }
    
    var startOfWeek: Date {
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        let dslTimeOffset = NSTimeZone.local.daylightSavingTimeOffset(for: date)
        return date.addingTimeInterval(dslTimeOffset)
    }
    
    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .second, value: 604799, to: self.startOfWeek)!
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
}

extension String{
    //    var localized: String {
    //        let path = Bundle.main.path(forResource: AppPreferences.shared.language.code, ofType: "lproj")
    //        let bundle = Bundle(path: path!)
    //        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    //    }
    var localized :  String {
//        let language = Language.all[SettingsManager.instance.languageIndex()].code
//        let path = Bundle.main.path(forResource: language, ofType: "lproj")
//        let bundle = Bundle(path: path!)!
//        let result = NSLocalizedString(self, tableName: "Localization", bundle: bundle, value: self, comment: self)
        return self
    }
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    static func amountFormat(_ price: Double?) -> String{
        let pr = price ?? 0
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSeparator = " "
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        let result = numberFormatter.string(from: pr as NSNumber)!
        return result
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    func luhnCheck() -> Bool {
        var sum = 0
        //let digitStrings = number.characters.reverse().map { String($0) }
        let digitStrings = String(self.reversed()) //String(number.reversed())
        
        for (index, char) in digitStrings.enumerated() {
            guard let digit = Int(String(char)) else { return false }
            let odd = index % 2 == 1
            
            switch (odd, digit) {
            case (true, 9):
                sum += 9
            case (true, 0...8):
                sum += (digit * 2) % 9
            default:
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    static func amountFormat(double : Double)->String{
        let formatter = NumberFormatter()
        let int = Int(double)
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        formatter.usesGroupingSeparator = true
        return formatter.string(from: int as NSNumber) ?? "0"
    }
    
    static func timeString(seconds: Int)->String{
        let sec: Int32 = Int32(seconds%60)
        let minutes: Int32 = Int32((seconds/60)%60)
        let hours: Int32 = Int32(seconds/3600)
        if seconds < 3600 {
            return String(format: "%02d:%02d",minutes,sec)
        } else{
            return  String(format: "%02d:%02d:%02d", hours,minutes,sec)
        }
    }
    
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
    static func stringFromJson(json:AnyObject)->String{
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            guard let convertedString = String(data: data1, encoding: String.Encoding.utf8) else { return "{}" } // the data will be converted to the string
            return convertedString
            //print(convertedString ?? "defaultvalue")
        } catch let myJSONError {
            print(myJSONError)
        }
        return "{}"
    }
    
    func convertToJson()->[String : AnyObject]{
        do{
            if let js = self.data(using: String.Encoding.utf8){
                if let jsonData = try JSONSerialization.jsonObject(with: js, options: .allowFragments) as? [String:AnyObject]{
                    return jsonData
                }
            }
        }catch {
            print(error.localizedDescription)
            
        }
        return [:]
    }
    
    
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
        let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
        return String(cleanedUpCopy.enumerated().map(){
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
        }.joined().dropFirst())
        //        return String(cleanedUpCopy.characters.enumerated().map() {
        //            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
        //        }.joined().dropFirst())
    }
    
}
extension UITableView{
    
    func addNib(name:String) -> Void {
        let nib = UINib(nibName: name, bundle: nil)
        self.register(nib, forCellReuseIdentifier: name)
    }
}
extension UICollectionView{
    func addNib(name:String) -> Void {
        let nib = UINib(nibName: name, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: name)
    }
}


extension UIButton{
    static public func nextBtn()->UIButton{
        let btn = UIButton()
        //btn.titleLabel?.font = UIFont(size: 20, bold: true)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(hex: "3a4ccb")
        btn.layer.cornerRadius = 26
        
        btn.layer.shadowColor = UIColor(red: 0.227, green: 0.298, blue: 0.796, alpha: 0.9).cgColor
        btn.layer.shadowOpacity = 1.0
        btn.layer.shadowOffset = CGSize(width: 0, height: 5)
        btn.layer.shadowRadius = 17
        return btn
    }
}
extension UIView {
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    class func fromNib<T:UIView>() -> T {
        let nibName = String(describing: self)
        if let nib = Bundle.main.loadNibNamed(nibName, owner: self),
           let nibView = nib.first as? T {
            return nibView
        }
        
        return T()
        
        //return Bundle(for: T.self).loadNibNamed(nibName, owner: nil, options: nil)![0] as! T
    }
    
    func showShadow()->Void{
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor(r: 200, g: 200, b: 200)?.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.cornerRadius = 10
        self.clipsToBounds = false
        self.layer.shadowOffset = CGSize(width: 12.0, height: 3.0)
    }
    
    func addBlurEffect()->Void{
        removeBlurEffect()
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    func removeBlurEffect() {
        let blurredEffectViews = self.subviews.filter{$0 is UIVisualEffectView}
        blurredEffectViews.forEach{ blurView in
            blurView.removeFromSuperview()
        }
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

class Constants {
    static let appColor = UIColor(hex: "3a4ccb")!
    static let appGreenColor = UIColor(r: 78, g: 172, b: 90)
    static let appBgColor = UIColor(r: 45, g: 62, b: 80)
    
    static let whiteModeBgColor = UIColor(r: 235, g: 235, b: 235)
    static let appBlueColor = UIColor(hex: "3a4ccb")!
    
    
    static let textGrayColor = UIColor(hex: "7a869a")
}

public struct UserDefaultKeys {
    static var darkMode = "dark"
}

class KeyChain {
    
    class func save(key: String, data: Data) -> OSStatus {
        let query = [
            kSecClass as String       : kSecClassGenericPassword as String,
            kSecAttrAccount as String : key,
            kSecValueData as String   : data ] as [String : Any]
        
        SecItemDelete(query as CFDictionary)
        
        return SecItemAdd(query as CFDictionary, nil)
    }
    
    class func load(key: String) -> Data? {
        let query = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key,
            kSecReturnData as String  : kCFBooleanTrue!,
            kSecMatchLimit as String  : kSecMatchLimitOne ] as [String : Any]
        
        var dataTypeRef: AnyObject? = nil
        
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        
        if status == noErr {
            return dataTypeRef as! Data?
        } else {
            return nil
        }
    }
    
    class func delete(key:String) -> Bool{
        let query: [String: Any] = [
            kSecClass as String       : kSecClassGenericPassword,
            kSecAttrAccount as String : key
        ]
        
        //        query = addAccessGroupWhenPresent(query)
        //        query = addSynchronizableIfRequired(query, addingItems: false)
        //        lastQueryParameters = query
        
        let status = SecItemDelete(query as CFDictionary)
        
        return status == noErr
    }
    
    class func createUniqueID() -> String {
        let uuid: CFUUID = CFUUIDCreate(nil)
        let cfStr: CFString = CFUUIDCreateString(nil, uuid)
        let swiftString: String = cfStr as String
        return swiftString
    }
}


extension Data {
    
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
    init<T>(from value: T) {
        var value = value
        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
    
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.load(as: T.self) }
    }
}

extension UITextField{
    func addLeft(left:CGFloat){
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: left, height: 5))
        self.leftViewMode = .always
    }
    
    func englishCharactersAndNumbers(count:Int, range: NSRange, string:String)->Bool{
        let inverseSet = NSCharacterSet(charactersIn:"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz").inverted
        let components = string.components(separatedBy: inverseSet)
        let filtered = components.joined(separator: "")
        if filtered != string{
            return false
        }
        guard let textFieldText = self.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let k = textFieldText.count - substringToReplace.count + string.count
        return k <= count
    }
    func defaultCharacters(count:Int, range: NSRange, string:String)->Bool{
        guard let textFieldText = self.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let k = textFieldText.count - substringToReplace.count + string.count
        return k <= count
    }
    
    func numbers(count:Int, range: NSRange, string:String)->Bool{
        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let components = string.components(separatedBy: inverseSet)
        let filtered = components.joined(separator: "")
        if filtered != string{
            return false
        }
        guard let textFieldText = self.text,
              let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let k = textFieldText.count - substringToReplace.count + string.count
        return k <= count
    }
    
}
