//
//  NetworkConstants.swift
//  iOS app
//
//  Created by ios developer on 25.05.2021.
//

import Foundation

struct AppURL {
    static var baseURL: String {
        return "https://api.github.com"
    }
    static var newsURL: String {
        return "https://www.haultail.com/wp-json/"
    }
    static var termsUrl: String {
        return "https://www.google.com/search?q=terms+of+service&rlz=1C5CHFA_enRU913RU913&sxsrf=ALeKk03Z8815KwNWt-igaTkylC6Th2IaQA%3A1625484523487&ei=6-ziYLmOHe_3qwGSmKKoBQ&oq=terms&gs_lcp=Cgdnd3Mtd2l6EAMYADIECAAQQzIECAAQQzIECAAQQzIECAAQQzICCAAyAggAMgQIABBDMgIIADIECAAQQzICCAA6BAgjECc6CAgAEAoQARBDOgoIABCxAxCDARBDOggIABCxAxCDAToFCAAQsQM6BQguELEDOggILhCxAxCDAToCCC5KBAhBGABQwp4BWMurAWDssgFoAXACeACAAZEBiAGdBpIBAzAuN5gBAKABAaoBB2d3cy13aXrAAQE&sclient=gws-wiz"
    }
    static var privacyUrl: String {
        return "https://www.google.com/search?q=privacy+policy&rlz=1C5CHFA_enRU913RU913&oq=privacy+pol&aqs=chrome.0.0l3j69i57j0l6.5701j0j15&sourceid=chrome&ie=UTF-8"
    }
}

struct Keychain {
    static let userId = "userId"
    static let registrationToken = "registrationToken"
    static let refreshToken = "refreshToken"
    static let guestToken = "guestToken"
    static let bearerToken = "bearerToken"
    static let newPassword = "newPassword"
    static let email = "email"
}

struct UserDefaultsKeys {
    static let  selectedCountryCode = "selectedCountryCode"
}

struct DefaultValues {
    static let defaultCountryCode = "{\"id\":3926,\"country_code\":\"RU\",\"country_name\":\"Russian Federation\",\"sequence\":0,\"dial\":\"+7\"}"
}
