//
//  Network.swift
//  iOS app
//
//  Created by ios developer on 25.05.2021.
//

import Foundation
import Moya
import ObjectMapper
import Alamofire

class Network {
    static let shared = Network()
    private var requests: [Cancellable] = []
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    let provider = MoyaProvider<MultiTarget>(plugins: [
        NetworkLoggerPlugin(verbose: true, cURL: true, output: nil, requestDataFormatter: nil, responseDataFormatter: nil),
        
    ])
    func request<T: BaseTargetType>(target: T, success successCallback: @escaping (Mappable, String) -> Void, error errorCallback: @escaping (AppError) -> Void) -> Cancellable {

        let request = provider.request(MultiTarget(target)) { [weak self] (result) in
            switch result { 
            case .success(let response):
                if response.statusCode >= 200 && response.statusCode < 300,
                    let responseString = String(data: response.data, encoding: String.Encoding.utf8) as String? {
                    if let responseObject = target.responseType.init(JSONString: responseString){
                        successCallback(responseObject, responseString)
                    }
                    if let responseObject = target.responseType.init(JSONString: responseString) {
                        successCallback(responseObject, responseString)
                    } else  {
                        successCallback(APIBaseObject(JSON: [:])!, responseString)
                    }
                } else {
                    if let responseString = String(data: response.data, encoding: String.Encoding.utf8) as String?,
                       let errorObject = AppError.init(JSONString: responseString) {
                        errorObject.responseCode = response.statusCode
                        errorCallback(errorObject)
                    } else {
                        errorCallback(AppError(JSON: ["message": "Parsing Error"])!)
                    }
                }
            case .failure(let error):
                errorCallback(AppError(JSON: ["message": error.errorDescription ?? ""])!)
            }
        }
        self.requests.append(request)
        return request
    }
    func request2<T: BaseTargetType>(target: T, success successCallback: @escaping ([Mappable], String) -> Void, error errorCallback: @escaping (AppError) -> Void) -> Cancellable {

        let request = provider.request(MultiTarget(target)) { [weak self] (result) in
            switch result {
            case .success(let response):
                if response.statusCode >= 200 && response.statusCode < 300{
                    do {
                        if let jsonArray = try JSONSerialization
                            .jsonObject(with: response.data, options: []) as? [[String: AnyObject]] {
                            var result = [Mappable]()
                            for element in jsonArray {
                                if let obj =  target.responseType.init(JSON: element){
                                    result.append(obj)
                                }
                            }
                            successCallback(result, "")
                            return
                        } else {
                            /* ... */
                        }
                    }
                    catch let error as NSError {
                        print(error)
                    }
                    successCallback([], "")
                } else {
                    if let responseString = String(data: response.data, encoding: String.Encoding.utf8) as String?,
                       let errorObject = AppError.init(JSONString: responseString) {
                        errorObject.responseCode = response.statusCode
                        errorCallback(errorObject)
                    } else {
                        errorCallback(AppError(JSON: ["message": "Parsing Error"])!)
                    }
                }
            case .failure(let error):
                errorCallback(AppError(JSON: ["message": error.errorDescription ?? ""])!)
            }
        }
        self.requests.append(request)
        return request
    }
    
//    func request2<T: BaseTargetType>(target: T, success successCallback: @escaping (Mappable, String) -> Void, error errorCallback: @escaping (AppError) -> Void) -> Cancellable {
//
//        let request = provider.request(MultiTarget(target)) { [weak self] (result) in
//            switch result {
//            case .success(let response):
//                if response.statusCode >= 200 && response.statusCode < 300,
//                    let responseString = String(data: response.data, encoding: String.Encoding.utf8) as String? {
//
//                    do {
//                        if let jsonArray = try JSONSerialization
//                            .jsonObject(with: response.data, options: []) as? [[String: AnyObject]] {
//                            var result = [Mappable]()
//                            for json in jsonArray{
//                                if let obj =  target.responseType.init(JSON: json){
//                                    result.append(obj)
//                                }
//                            }
//                            successCallback(result, responseString)
//
//                        } else {
//                            /* ... */
//                        }
//                    }
//                    catch let error as? Error {
//
//                    }
//                    successCallback([APIBaseObject(JSON: [:])], responseString)
//
//                } else {
//                    if let responseString = String(data: response.data, encoding: String.Encoding.utf8) as String?,
//                       let errorObject = AppError.init(JSONString: responseString) {
//                        errorObject.responseCode = response.statusCode
//                        errorCallback(errorObject)
//                    } else {
//                        errorCallback(AppError(JSON: ["message": "Parsing Error"])!)
//                    }
//                }
//            case .failure(let error):
//                errorCallback(AppError(JSON: ["message": error.errorDescription ?? ""])!)
//            }
//        }
//        self.requests.append(request)
//        return request
//    }
    
    func cancelAll() {
        requests.forEach({$0.cancel()})
    }
    
    func refreshToken<T: BaseTargetType>(target: T, success successCallback: @escaping (Mappable, String) -> Void, error errorCallback: @escaping (AppError) -> Void) {
        
    }
    
    func startNetworkReachabilityObserver() {

        reachabilityManager?.listener = { status in
            switch status {
                case .notReachable:
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.showInternetConnectionError()
                    break
                default:
                    break
                }
            }

            // start listening
            reachabilityManager?.startListening()
       }
}


