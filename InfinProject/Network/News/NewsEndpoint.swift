//
//  NewsEndpoint.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 03/08/21.
//

import UIKit
import ObjectMapper
import Moya

enum NewsEndpoint {
    case news(offset:Int, count:Int)
}

extension NewsEndpoint: BaseTargetType {

    var responseType: Mappable.Type {
        switch self {
        case .news:
            return NewsResponse.self
        }
    }
    
    var baseURL: URL {
        return URL(string: AppURL.newsURL)!
    }
    
    var path: String {
        switch self {
        case .news(let offset, let count):
        //haultail_news/v1/posts/${count}/${offset}`
            return "haultail_news/v1/posts/"+"\(count)" + "/" + "\(offset)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .news:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var headers = ["X-OS-VERSION": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)",
                       "X-DEVICE": UIDevice.current.identifierForVendor?.uuidString ?? ""]
        
        switch self {
        default:
            headers["Content-Type"] = "application/json"
            headers["Accept"] = "application/json"
            break
        }
        
        return headers
    }
}

