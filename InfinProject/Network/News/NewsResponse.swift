//
//  NewsResponse.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 03/08/21.
//

import UIKit
import ObjectMapper

class NewsResponse: APIBaseObject {
    
    var postid: Int?
    var title: String?
    var content: String?
    var thumbnail: String?
    var image : String?
    var date:Date?
    var dateJsonStr:String?
    var dateString:String?
    var htmlContent : NSAttributedString?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        postid <- map["post_ID"]
        title <- map["post_title"]
        thumbnail <- map["post_thumbnail"]
        image <- map["post_image"]
        content <- map["post_content"]
        dateJsonStr <- map["post_date"]
        if dateJsonStr != nil{
            self.date = Date.date(string: dateJsonStr, format: "yyyy-MM-dd HH:mm:ss")
            dateString = self.date?.string(format: "dd MMM yyyy")
        }       
    }

}

/*
 "post_ID":8691,"post_title":"The Best Junk Removal Tools in the Market","post_thumbnail":"https:\/\/www.haultail.com\/wp-content\/uploads\/2021\/07\/\u0421\u043d\u0438\u043c\u043e\u043a-\u044d\u043a\u0440\u0430\u043d\u0430-2021-07-31-\u0432-20.18.47-97x97.png","post_image":"https:\/\/www.haultail.com\/wp-content\/uploads\/2021\/07\/\u0421\u043d\u0438\u043c\u043e\u043a-\u044d\u043a\u0440\u0430\u043d\u0430-2021-07-31-\u0432-20.18.47-320x178.png","post_content":"<h2><span
 "post_date":"2021-07-31 17:20:09"}
 */
