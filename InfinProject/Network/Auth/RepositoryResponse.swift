//
//  RepositoryResponse.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import ObjectMapper

class RepositoryResponse: APIBaseObject{
    var total_count: Int = 0
    var items:[RepositoryRO]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        items <- map["items"]
        total_count <- map["total_count"]
    }
}

class RepositoryRO: APIBaseObject {
    var owner: RepositoryOwnerResponse?
    var url: String?
    var name: String?
    var language: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        owner <- map["owner"]
        url <- map["html_url"]
        language <- map["language"]
        name <- map["full_name"]
    }
}

