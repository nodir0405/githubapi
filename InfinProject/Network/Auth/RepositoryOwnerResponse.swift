//
//  RepOwner.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import ObjectMapper

class RepositoryOwnerResponse: APIBaseObject {
    
    var url: String?
    var login: String?
    var avatar: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        url <- map["html_url"]
        avatar <- map["avatar_url"]
        login <- map["login"]
    }
}
