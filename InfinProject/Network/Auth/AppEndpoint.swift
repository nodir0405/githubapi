//
//  Auth.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import ObjectMapper
import Moya

enum AppEndpoint {
    case login(username: String, password:String)
    case search(searchText: String)
    case nextPage(searchText: String, page:Int, count:Int)
}

extension AppEndpoint: BaseTargetType {

    var responseType: Mappable.Type {
        switch self {
        case .login:
            return APIBaseObject.self
        case .search:
            return RepositoryResponse.self
        case .nextPage:
            return RepositoryResponse.self
        
        }
    }
    
    var baseURL: URL {
        return URL(string: AppURL.baseURL)!
    }
    
    var path: String {
        switch self {
        case .login(let username, let password):
            return "users/\(username)"
        case .search:
            return "search/repositories"
        case .nextPage:
            return "search/repositories"
        
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .search(let search):
            //return .requestParameters(parameters: ["q": search], encoding: JSONEncoding.default)
            return .requestParameters(parameters: ["q": search], encoding: URLEncoding.default)
        case .nextPage(let searchText, let page, let count):
            let params = ["page":page,
                          "q":searchText,
                          "per_page":count] as [String : Any]
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        var headers = ["X-OS-VERSION": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)",
                       "X-DEVICE": UIDevice.current.identifierForVendor?.uuidString ?? ""]
        
        switch self {
        default:
            headers["Content-Type"] = "application/json"
            headers["Accept"] = "application/vnd.github.v3+json"
            //headers["Authorization"] = "Bearer \(JJUserManager.shared.token ?? "")"
            break
        }
        
        return headers
    }
}

