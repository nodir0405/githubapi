//
//  BaseTargetType.swift
//  iOS app
//
//  Created by ios developer on 25.05.2021.
//

import Foundation
import Moya
import ObjectMapper

protocol BaseTargetType: TargetType {
    var responseType: Mappable.Type { get }
}
