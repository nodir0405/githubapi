//
//  Error.swift
//  iOS app
//
//  Created by ios developer on 27.05.2021.
//

import Foundation
import ObjectMapper

class JJViolation: Mappable {
    var propertyPath: String?
    var message: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        message <- map["message"]
        propertyPath <- map["propertyPath"]
    }
}

class AppError: Mappable {
    var message: String?
    var violations: [JJViolation]?
    var extraMessage: Int?
    var responseCode: Int?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        message <- map["message"]
        violations <- map["violations"] 
        extraMessage <- map["extra_message"]
    }
    
    class func unknown() -> AppError {
        return AppError(JSON: ["message" : "unknown error"])!
    }
}
