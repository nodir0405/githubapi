//
//  AppDelegate.swift
//  InfinProject
//
//  Created by Jurayev Nodir on 23/07/21.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import CoreData
import FirebaseAuth


@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        notificationEnabled()
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = true
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        if let token = UserManager.shared.bearerToken{
            openHome()
        } else{
            openLogin()
        }
        return true
    }

    func openHome(){
        window?.rootViewController = UINavigationController(rootViewController: HomeViewController())
    }
    func openLogin(){
        window?.rootViewController = UINavigationController(rootViewController: LoginViewController())
    }
    func showInternetConnectionError()  {
        guard let nvc = UIApplication.shared.windows.first?.rootViewController else {
            return
        }
        guard let errorVC = UIStoryboard(name: "InternetConnectionError", bundle: nil).instantiateInitialViewController() else {
            return
        }
        errorVC.modalPresentationStyle = .fullScreen
        if let topController = nvc.presentedViewController  {
            topController.dismiss(animated: false) {
                nvc.present(errorVC, animated: true, completion: nil)
            }
        } else {
            nvc.present(errorVC, animated: true, completion: nil)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.unknown)
    }
    
    
    func application(_ application: UIApplication , didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        if Auth.auth().canHandleNotification(notification)
        {
            completionHandler(UIBackgroundFetchResult.noData);
            return
        }
    }
    func notificationEnabled()->Void{
        let application = UIApplication.shared
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
    }
    
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return true
    }
    
    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        return true
    }

}

